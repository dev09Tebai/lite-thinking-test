import uuid

from django.db import models
from django.db import models
from django.utils import timezone

class Company(models.Model):

  id = models.UUIDField(primary_key=True, default=uuid.uuid4)
  name = models.CharField(max_length=200)
  location = models.CharField(max_length=300)
  nit = models.IntegerField()
  phone = models.CharField(max_length=200)
  created_date = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.name

