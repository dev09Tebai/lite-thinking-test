
from django.urls import include, path
from . import views

urlpatterns = [
  path('companies/get', views.get_companies),
  path('companies/add', views.add_company),
  path('companies/edit/<uuid:company_id>', views.update_company),
  path('companies/delete/<uuid:company_id>', views.delete_company)
]
