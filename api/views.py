import json

from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view

from .models import Company
from .serializers import CompanySerializer

@api_view(["GET"])
def get_companies(request):
    companies = Company.objects.all()
    serializer = CompanySerializer(companies, many=True)
    return JsonResponse({'companies': serializer.data}, safe=False, status=status.HTTP_200_OK)

@api_view(["POST"])
def add_company(request):
    payload = json.loads(request.body)
    try:
        Company.objects.create(
            name=payload["name"],
            location=payload["location"],
            nit=payload["nit"],
            phone=payload["phone"],
        )
        return JsonResponse({'message': 'Company was registered successfully'}, safe=False, status=status.HTTP_201_CREATED)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception as ex:
        return JsonResponse({'error': ex}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(["PUT"])
def update_company(request, company_id):
    payload = json.loads(request.body)
    try:
        company_item = Company.objects.filter(id=company_id)
        company_item.update(**payload)
        company_updated = Company.objects.get(id=company_id)
        serializer = CompanySerializer(company_updated)
        return JsonResponse({'company': serializer.data}, safe=False, status=status.HTTP_200_OK)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Houston, we have a lite problem'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(["DELETE"])
def delete_company(request,company_id):
    try:
        company = Company.objects.get(id=company_id)
        company.delete()
        return JsonResponse({'message':'company deleted'},status=status.HTTP_204_NO_CONTENT)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Houston,we have a lite problem'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
